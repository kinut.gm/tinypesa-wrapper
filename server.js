import axios from "axios"
import express from "express"
import bodyParser from "body-parser"
import cors from "cors"

let port = 3001 || process.env.PORT
const app = express()
let confirmation = null

app.use(bodyParser.json())
app.use(cors())

app.listen(port, () => {
  console.log(`Payment server running on port: ${port}`)
})

app.post("/callback", (req, res) => {
  res.status(200).end()
  confirmation = req.body.Body.stkCallback
})

const returnConfirm = () => {
  return new Promise((resolve, reject) => {
    setInterval(() => {
      if (!confirmation) return
      if (confirmation) {
        resolve(confirmation)
      }
    }, 500)
  })
}

app.post("/send", (req, res) => {
  const { ApiKey, amount, msisdn, account_no } = req.body

  axios({
    url: "https://tinypesa.com/api/v1/express/initialize",
    method: "post",
    data: { amount, msisdn, account_no },
    headers: {
      ApiKey,
    },
  }).then(() => {
    returnConfirm().then((_confirmation) => {
      res.send(_confirmation)
    })
  })
})
